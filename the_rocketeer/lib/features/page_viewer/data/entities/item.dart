import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';

part 'item.freezed.dart';
part 'item.g.dart';

@freezed
class Item with _$Item {
  factory Item({
    required String id,
    required String title,
    required String imageUrl,
    required String body,
    required String source,
  }) = _Item;

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);
}

extension ItemExtension on Item {
  List<Item> getAllSubChildren(HomeData homeData) {
    List<Item> items = [];
    RegExp regExp = RegExp(
      "[(]openitem:([A-Za-z0-9]+)[)]",
      caseSensitive: false,
    );

    if (regExp.hasMatch(body)) {
      regExp.allMatches(body).forEach((element) {
        final item = homeData.fetchItemById(element.group(1).toString());
        if (item != null) {
          items.add(item);
          items.addAll(item.getAllSubChildren(homeData));
        }
      });
    }

    return items;
  }
}
