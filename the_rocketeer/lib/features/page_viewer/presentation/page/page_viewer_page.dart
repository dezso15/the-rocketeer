import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:the_rocketeer/core/router/router.gr.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';
import 'package:the_rocketeer/core/widgets/rocketeer_scaffold.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:the_rocketeer/features/page_viewer/data/entities/item.dart';
import 'package:the_rocketeer/features/page_viewer/presentation/widgets/link_preview.dart';

class PageViewerPage extends StatelessWidget {
  final String itemId;
  final HomeData homeData;
  const PageViewerPage({required this.itemId, required this.homeData});

  @override
  Widget build(BuildContext context) {
    final item = homeData.fetchItemById(itemId);

    if (item == null) {
      return const Center(
        child: Text("Item not found"),
      );
    }

    return RocketeerScaffold(
      isScrollable: true,
      child: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Text(
              item.title,
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    letterSpacing: 1.6,
                  ),
            ),
            const SizedBox(height: 20),
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                item.imageUrl,
                width: UIHelper.deviceWidth * 0.8,
                fit: BoxFit.fitWidth,
              ),
            ),
            const SizedBox(height: 16),
            _Body(
              homeData: homeData,
              item: item,
            ),
            const SizedBox(height: 20),
            SelectableText(item.source),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final Item item;
  final HomeData homeData;
  const _Body({required this.item, required this.homeData, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bodyTextTheme = Theme.of(context).textTheme.bodyText1!;
    return Html(
      customRender: {
        "a": (renderContext, widget) {
          if (renderContext.tree.element != null) {
            if (renderContext.tree.element!.attributes.containsKey("href")) {
              final href = renderContext.tree.element!.attributes["href"];
              if (href!.contains("openitem:")) {
                final selectedItemId = href.split(":").last;
                return _Link(homeData: homeData, itemId: selectedItemId, widget: widget);
              }
            }
          }
          return widget;
        }
      },
      style: {
        "p": Style(
          textAlign: TextAlign.justify,
          wordSpacing: bodyTextTheme.wordSpacing,
          fontSize: FontSize(bodyTextTheme.fontSize),
          lineHeight: LineHeight(bodyTextTheme.height),
        ),
      },
      data: md.markdownToHtml(
        item.body,
      ),
    );
  }
}

class _Link extends StatelessWidget {
  final Widget widget;
  final HomeData homeData;
  final String itemId;
  const _Link({required this.widget, required this.homeData, required this.itemId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          context: context,
          builder: (ctx) {
            return LinkPreview(homeData: homeData, itemId: itemId);
          },
        );
      },
      onTap: () {
        AutoRouter.of(context).push(
          PageViewerPageRoute(itemId: itemId, homeData: homeData),
        );
      },
      child: widget,
    );
  }
}
