import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';

class LinkPreview extends StatelessWidget {
  final String itemId;
  final HomeData homeData;
  const LinkPreview({Key? key, required this.homeData, required this.itemId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final item = homeData.fetchItemById(itemId);
    if (item != null) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 20),
          Text(
            item.title,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  letterSpacing: 1.6,
                ),
          ),
          const SizedBox(height: 20),
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              item.imageUrl,
              width: UIHelper.deviceWidth * 0.8,
              height: 200,
              fit: BoxFit.fitWidth,
            ),
          ),
          const SizedBox(height: 16),
          SizedBox(
            height: 100,
            child: Html(
              data: md.markdownToHtml(
                item.body,
              ),
              style: {
                "p": Style(
                  textAlign: TextAlign.justify,
                  textOverflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              },
            ),
          )
        ],
      );
    }
    return const Text("Item not found");
  }
}
