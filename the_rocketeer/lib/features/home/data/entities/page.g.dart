// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Page _$$_PageFromJson(Map<String, dynamic> json) => _$_Page(
      id: json['id'] as String,
      title: json['title'] as String,
      itemId: json['itemId'] as String,
    );

Map<String, dynamic> _$$_PageToJson(_$_Page instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'itemId': instance.itemId,
    };
