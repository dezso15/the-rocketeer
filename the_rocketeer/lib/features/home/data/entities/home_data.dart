import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:the_rocketeer/features/home/data/entities/page.dart';
import 'package:the_rocketeer/features/page_viewer/data/entities/item.dart';

part 'home_data.freezed.dart';
part 'home_data.g.dart';

@freezed
class HomeData with _$HomeData {
  factory HomeData({
    required List<Page> pages,
    required List<Item> items,
  }) = _HomeData;

  factory HomeData.fromJson(Map<String, dynamic> json) => _$HomeDataFromJson(json);
}

extension HomeDataExtension on HomeData {
  Item? getDirectlyConnectedItemByPage(Page page) {
    return items.firstWhereOrNull((item) => item.id == page.itemId);
  }

  Item? fetchItemById(String id) {
    return items.firstWhereOrNull((item) => item.id == id);
  }

  List<Item> getConnectedItemsByPage(Page page) {
    List<Item> connectedItems = [];
    final firstItem = getDirectlyConnectedItemByPage(page);
    if (firstItem != null) {
      connectedItems.add(firstItem);
      connectedItems.addAll(firstItem.getAllSubChildren(this));
    }
    return connectedItems;
  }
}
