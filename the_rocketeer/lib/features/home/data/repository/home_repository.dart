import 'package:dio/dio.dart';
import 'package:the_rocketeer/core/server/servers.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';

class HomeRepository {
  static final HomeRepository _instance = HomeRepository._internal();
  factory HomeRepository() => _instance;

  HomeRepository._internal();

  Future<HomeData> getHomeData(String id) async {
    Response response = await servers.simpleClub.get("navigating-dynamic-content/" + id);
    return HomeData.fromJson(response.data);
  }
}
