import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';
import 'package:the_rocketeer/core/widgets/error_snack.dart';
import 'package:the_rocketeer/core/widgets/loading_animation.dart';
import 'package:the_rocketeer/core/widgets/rocketeer_scaffold.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';
import 'package:the_rocketeer/features/home/data/repository/home_repository.dart';
import 'package:the_rocketeer/features/home/presentation/cubit/home_cubit.dart';
import 'package:the_rocketeer/features/home/presentation/cubit/home_cubit_events.dart';
import 'package:the_rocketeer/features/home/presentation/widgets/search_result.dart';
import 'package:the_rocketeer/features/home/presentation/widgets/search_widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) => HomeCubit(HomeRepository()),
      child: BlocConsumer<HomeCubit, HomeCubitState>(
        listener: (ctx, state) {
          state.whenOrNull(error: (err) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: ErrorSnack(error: err),
              ),
            );
          });
        },
        builder: (ctx, state) {
          return WillPopScope(
            onWillPop: () {
              return BlocProvider.of<HomeCubit>(ctx).mapEventToState(const HomeCubitEvents.onBackButtonPress());
            },
            child: RocketeerScaffold(
              child: state.when(
                init: () => const _Init(),
                loading: () => const _Loading(),
                loaded: (homeData, searchTerm) => _Loaded(homeData: homeData, searchTerm: searchTerm),
                error: (err) => const _Init(),
              ),
            ),
          );
        },
      ),
    );
  }
}

class _Loaded extends StatelessWidget {
  final HomeData homeData;
  final String searchTerm;
  const _Loaded({required this.homeData, required this.searchTerm, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(height: 25),
        SearchWidget(searchTerm: searchTerm),
        Expanded(
          child: SingleChildScrollView(
            child: SearchResult(homeData),
          ),
        ),
      ],
    );
  }
}

class _Init extends StatelessWidget {
  const _Init({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "assets/images/therocketeer_icon.png",
          height: UIHelper.deviceHeight * 0.34,
        ),
        const SizedBox(height: 20),
        SearchWidget(),
      ],
    );
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        width: 300,
        height: 300,
        child: LoadingAnimation(),
      ),
    );
  }
}
