import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';

part 'home_cubit_state.freezed.dart';

@freezed
abstract class HomeCubitState with _$HomeCubitState {
  const factory HomeCubitState.init() = _InitState;
  const factory HomeCubitState.loading() = _LoadingState;
  const factory HomeCubitState.loaded(HomeData homeData, String searchTerm) = _LoadedState;
  const factory HomeCubitState.error(String error) = _ErrorState;
}
