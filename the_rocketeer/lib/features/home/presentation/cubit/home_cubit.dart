import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_rocketeer/features/home/data/repository/home_repository.dart';
import 'package:the_rocketeer/features/home/presentation/cubit/home_cubit_events.dart';

import 'home_cubit_state.dart';
export 'home_cubit_state.dart';

class HomeCubit extends Cubit<HomeCubitState> {
  final HomeRepository _homeRepository;

  HomeCubit(this._homeRepository) : super(const HomeCubitState.init());

  dynamic mapEventToState(HomeCubitEvents events) {
    return events.map(
      search: _search,
      onBackButtonPress: _onBackButtonPress,
    );
  }

  Future<Object?> _search(HomeCubitSearchEvent event) async {
    emit(const HomeCubitState.loading());
    try {
      final content = await _homeRepository.getHomeData(event.id);
      emit(HomeCubitState.loaded(content, event.id));
    } catch (error) {
      emit(HomeCubitState.error("There is no result for " + event.id + "!"));
    }
  }

  Future<bool> _onBackButtonPress(HomeCubitOnBackButtonPress event) async {
    if (const HomeCubitState.init().runtimeType == state.runtimeType) {
      return Future.value(true);
    } else {
      emit(const HomeCubitState.init());
      return Future.value(false);
    }
  }
}
