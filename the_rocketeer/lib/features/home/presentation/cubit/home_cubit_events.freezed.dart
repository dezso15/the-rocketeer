// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_cubit_events.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$HomeCubitEventsTearOff {
  const _$HomeCubitEventsTearOff();

  HomeCubitSearchEvent search(String id) {
    return HomeCubitSearchEvent(
      id,
    );
  }

  HomeCubitOnBackButtonPress onBackButtonPress() {
    return const HomeCubitOnBackButtonPress();
  }
}

/// @nodoc
const $HomeCubitEvents = _$HomeCubitEventsTearOff();

/// @nodoc
mixin _$HomeCubitEvents {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id) search,
    required TResult Function() onBackButtonPress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeCubitSearchEvent value) search,
    required TResult Function(HomeCubitOnBackButtonPress value)
        onBackButtonPress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeCubitEventsCopyWith<$Res> {
  factory $HomeCubitEventsCopyWith(
          HomeCubitEvents value, $Res Function(HomeCubitEvents) then) =
      _$HomeCubitEventsCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeCubitEventsCopyWithImpl<$Res>
    implements $HomeCubitEventsCopyWith<$Res> {
  _$HomeCubitEventsCopyWithImpl(this._value, this._then);

  final HomeCubitEvents _value;
  // ignore: unused_field
  final $Res Function(HomeCubitEvents) _then;
}

/// @nodoc
abstract class $HomeCubitSearchEventCopyWith<$Res> {
  factory $HomeCubitSearchEventCopyWith(HomeCubitSearchEvent value,
          $Res Function(HomeCubitSearchEvent) then) =
      _$HomeCubitSearchEventCopyWithImpl<$Res>;
  $Res call({String id});
}

/// @nodoc
class _$HomeCubitSearchEventCopyWithImpl<$Res>
    extends _$HomeCubitEventsCopyWithImpl<$Res>
    implements $HomeCubitSearchEventCopyWith<$Res> {
  _$HomeCubitSearchEventCopyWithImpl(
      HomeCubitSearchEvent _value, $Res Function(HomeCubitSearchEvent) _then)
      : super(_value, (v) => _then(v as HomeCubitSearchEvent));

  @override
  HomeCubitSearchEvent get _value => super._value as HomeCubitSearchEvent;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(HomeCubitSearchEvent(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$HomeCubitSearchEvent implements HomeCubitSearchEvent {
  const _$HomeCubitSearchEvent(this.id);

  @override
  final String id;

  @override
  String toString() {
    return 'HomeCubitEvents.search(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeCubitSearchEvent &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(id);

  @JsonKey(ignore: true)
  @override
  $HomeCubitSearchEventCopyWith<HomeCubitSearchEvent> get copyWith =>
      _$HomeCubitSearchEventCopyWithImpl<HomeCubitSearchEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id) search,
    required TResult Function() onBackButtonPress,
  }) {
    return search(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
  }) {
    return search?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
    required TResult orElse(),
  }) {
    if (search != null) {
      return search(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeCubitSearchEvent value) search,
    required TResult Function(HomeCubitOnBackButtonPress value)
        onBackButtonPress,
  }) {
    return search(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
  }) {
    return search?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
    required TResult orElse(),
  }) {
    if (search != null) {
      return search(this);
    }
    return orElse();
  }
}

abstract class HomeCubitSearchEvent implements HomeCubitEvents {
  const factory HomeCubitSearchEvent(String id) = _$HomeCubitSearchEvent;

  String get id => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HomeCubitSearchEventCopyWith<HomeCubitSearchEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeCubitOnBackButtonPressCopyWith<$Res> {
  factory $HomeCubitOnBackButtonPressCopyWith(HomeCubitOnBackButtonPress value,
          $Res Function(HomeCubitOnBackButtonPress) then) =
      _$HomeCubitOnBackButtonPressCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeCubitOnBackButtonPressCopyWithImpl<$Res>
    extends _$HomeCubitEventsCopyWithImpl<$Res>
    implements $HomeCubitOnBackButtonPressCopyWith<$Res> {
  _$HomeCubitOnBackButtonPressCopyWithImpl(HomeCubitOnBackButtonPress _value,
      $Res Function(HomeCubitOnBackButtonPress) _then)
      : super(_value, (v) => _then(v as HomeCubitOnBackButtonPress));

  @override
  HomeCubitOnBackButtonPress get _value =>
      super._value as HomeCubitOnBackButtonPress;
}

/// @nodoc

class _$HomeCubitOnBackButtonPress implements HomeCubitOnBackButtonPress {
  const _$HomeCubitOnBackButtonPress();

  @override
  String toString() {
    return 'HomeCubitEvents.onBackButtonPress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is HomeCubitOnBackButtonPress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id) search,
    required TResult Function() onBackButtonPress,
  }) {
    return onBackButtonPress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
  }) {
    return onBackButtonPress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id)? search,
    TResult Function()? onBackButtonPress,
    required TResult orElse(),
  }) {
    if (onBackButtonPress != null) {
      return onBackButtonPress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeCubitSearchEvent value) search,
    required TResult Function(HomeCubitOnBackButtonPress value)
        onBackButtonPress,
  }) {
    return onBackButtonPress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
  }) {
    return onBackButtonPress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeCubitSearchEvent value)? search,
    TResult Function(HomeCubitOnBackButtonPress value)? onBackButtonPress,
    required TResult orElse(),
  }) {
    if (onBackButtonPress != null) {
      return onBackButtonPress(this);
    }
    return orElse();
  }
}

abstract class HomeCubitOnBackButtonPress implements HomeCubitEvents {
  const factory HomeCubitOnBackButtonPress() = _$HomeCubitOnBackButtonPress;
}
