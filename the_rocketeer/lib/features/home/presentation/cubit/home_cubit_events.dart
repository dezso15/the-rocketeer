import 'package:freezed_annotation/freezed_annotation.dart';
part 'home_cubit_events.freezed.dart';

@freezed
class HomeCubitEvents with _$HomeCubitEvents {
  const factory HomeCubitEvents.search(String id) = HomeCubitSearchEvent;
  const factory HomeCubitEvents.onBackButtonPress() = HomeCubitOnBackButtonPress;
}
