import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart' hide Page;
import 'package:the_rocketeer/core/router/router.gr.dart';
import 'package:the_rocketeer/core/widgets/animated_openable_container.dart';
import 'package:the_rocketeer/features/home/presentation/widgets/card_image.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';
import 'package:the_rocketeer/features/home/data/entities/page.dart';
import 'package:the_rocketeer/features/page_viewer/data/entities/item.dart';

class SearchResult extends StatelessWidget {
  final HomeData homeData;
  const SearchResult(this.homeData, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (Page page in homeData.pages)
          _PageCard(
            page: page,
            homeData: homeData,
          ),
      ],
    );
  }
}

class _PageCard extends StatelessWidget {
  final Page page;
  final HomeData homeData;
  _PageCard({required this.page, required this.homeData, Key? key}) : super(key: key);

  final GlobalKey<AnimatedOpenableContainerState> animatedContainerKey = GlobalKey<AnimatedOpenableContainerState>();

  @override
  Widget build(BuildContext context) {
    Item? childItem = homeData.getDirectlyConnectedItemByPage(page);
    if (childItem != null) {
      return GestureDetector(
        onTap: () {
          AutoRouter.of(context).push(PageViewerPageRoute(itemId: childItem.id, homeData: homeData));
        },
        onLongPress: () {
          animatedContainerKey.currentState!.changeState();
        },
        child: CardImage(
          page: page,
          item: childItem,
          homeData: homeData,
          animatedContainerKey: animatedContainerKey,
        ),
      );
    }

    return Container();
  }
}
