import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart' hide Page;
import 'package:palette_generator/palette_generator.dart';
import 'package:the_rocketeer/core/router/router.gr.dart';
import 'package:the_rocketeer/core/util/utils.dart';
import 'package:the_rocketeer/core/widgets/animated_openable_container.dart';
import 'package:the_rocketeer/features/home/data/entities/home_data.dart';
import 'package:the_rocketeer/features/home/data/entities/page.dart';
import 'package:the_rocketeer/features/page_viewer/data/entities/item.dart';

class CardImage extends StatelessWidget {
  final Page page;
  final Item item;
  final HomeData homeData;
  final GlobalKey<AnimatedOpenableContainerState> animatedContainerKey;

  const CardImage({
    required this.page,
    required this.item,
    required this.animatedContainerKey,
    required this.homeData,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<PaletteGenerator>(
      future: Utils.paletteGeneratorFrom(item.imageUrl),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          Color dominantColor = snapshot.data!.dominantColor!.color;
          Color complementerColor = Utils.getComplementaryColor(dominantColor);

          return Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Card(
              shape: RoundedRectangleBorder(
                side: const BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              color: dominantColor,
              margin: EdgeInsets.zero,
              child: Column(
                children: [
                  _HeaderTitle(
                    title: page.title,
                    item: item,
                    complementerColor: complementerColor,
                    dominantColor: dominantColor,
                  ),
                  AnimatedOpenableContainer(
                    maxHeight: 200,
                    key: animatedContainerKey,
                    childWidget: ListView(
                      children: [
                        for (Item item in homeData.getConnectedItemsByPage(page))
                          InkWell(
                            onTap: () {
                              AutoRouter.of(context).push(PageViewerPageRoute(itemId: item.id, homeData: homeData));
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                              child: Text(
                                item.title,
                                style: Theme.of(context).textTheme.bodyText1!.copyWith(color: complementerColor),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        return Container();
      },
    );
  }
}

class _HeaderTitle extends StatelessWidget {
  final String title;
  final Item item;
  final Color dominantColor;
  final Color complementerColor;

  const _HeaderTitle({
    Key? key,
    required this.title,
    required this.item,
    required this.dominantColor,
    required this.complementerColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        image: DecorationImage(
          fit: BoxFit.fitWidth,
          alignment: Alignment.lerp(Alignment.center, Alignment.topCenter, 0.4)!,
          image: NetworkImage(item.imageUrl),
        ),
      ),
      width: double.infinity,
      height: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: dominantColor.withOpacity(0.55),
            ),
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline2!.copyWith(color: complementerColor),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: dominantColor.withOpacity(0.55),
            ),
            child: Text(
              item.title,
              style: Theme.of(context).textTheme.headline3!.copyWith(color: complementerColor),
            ),
          ),
        ],
      ),
    );
  }
}
