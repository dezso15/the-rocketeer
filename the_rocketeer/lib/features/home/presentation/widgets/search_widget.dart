import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';
import 'package:the_rocketeer/features/home/presentation/cubit/home_cubit.dart';
import 'package:the_rocketeer/features/home/presentation/cubit/home_cubit_events.dart';

class SearchWidget extends StatelessWidget {
  final String? searchTerm;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final TextEditingController _searchController = TextEditingController(text: searchTerm ?? "");

  SearchWidget({this.searchTerm, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 70,
            width: UIHelper.deviceWidth * 0.75,
            child: TextFormField(
              style: TextStyle(color: Colors.white.withOpacity(0.75)),
              cursorHeight: 25,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                  width: 2,
                  color: Colors.blueGrey,
                )),
                contentPadding: EdgeInsets.only(left: 10, right: 10),
              ),
              onFieldSubmitted: (text) {
                if (_formKey.currentState!.validate()) {
                  BlocProvider.of<HomeCubit>(context).mapEventToState(
                    HomeCubitEvents.search(text),
                  );
                }
              },
              validator: (text) {
                if (text?.isEmpty ?? true) {
                  return "The search term is too short!";
                }

                return null;
              },
              controller: _searchController,
            ),
          ),
          const SizedBox(width: 12),
          Container(
            width: 47,
            height: 47,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(5),
            ),
            child: InkWell(
              onTap: () {
                if (_formKey.currentState!.validate()) {
                  BlocProvider.of<HomeCubit>(context).mapEventToState(
                    HomeCubitEvents.search(_searchController.text),
                  );
                }
              },
              child: const Center(
                child: Icon(Icons.search),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
