import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:the_rocketeer/features/home/presentation/page/home_page.dart';
import 'package:the_rocketeer/features/page_viewer/presentation/page/page_viewer_page.dart';

@MaterialAutoRouter(
  routes: [
    CustomRoute(
      page: HomePage,
      initial: true,
      path: "home",
      maintainState: true,
      transitionsBuilder: RouteAnimation.none,
    ),
    CustomRoute(
      durationInMilliseconds: 500,
      reverseDurationInMilliseconds: 500,
      page: PageViewerPage,
      initial: false,
      path: "page",
      maintainState: true,
      transitionsBuilder: RouteAnimation.rocketeer,
    ),
  ],
)
class $AppRouter {}

class RouteAnimation {
  static const RouteTransitionsBuilder none = _none;
  static const RouteTransitionsBuilder rocketeer = _rocketeer;

  static Widget _none(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: Offset.zero,
        end: Offset.zero,
      ).animate(animation),
      child: child,
    );
  }

  static Widget _rocketeer(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    String rocketeerImage = "assets/images/rocketeer_right.png";
    if (animation.status == AnimationStatus.reverse || animation.status == AnimationStatus.dismissed) {
      rocketeerImage = "assets/images/rocketeer_left.png";
    }

    return Stack(
      children: [
        SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(-1, 0),
            end: Offset.zero,
          ).animate(animation),
          child: child,
        ),
        SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(-1, 0),
            end: const Offset(1, 0),
          ).animate(animation),
          child: Center(
            child: SizedBox(
              width: 100,
              height: 100,
              child: Transform(
                alignment: Alignment.center,
                transform: Matrix4.rotationY(0),
                child: Image.asset(rocketeerImage),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
