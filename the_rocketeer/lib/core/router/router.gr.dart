// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../../features/home/data/entities/home_data.dart' as _i6;
import '../../features/home/presentation/page/home_page.dart' as _i1;
import '../../features/page_viewer/presentation/page/page_viewer_page.dart'
    as _i2;
import 'router.dart' as _i5;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    HomePageRoute.name: (routeData) {
      return _i3.CustomPage<dynamic>(
          routeData: routeData,
          child: _i1.HomePage(),
          transitionsBuilder: _i5.RouteAnimation.none,
          opaque: true,
          barrierDismissible: false);
    },
    PageViewerPageRoute.name: (routeData) {
      final args = routeData.argsAs<PageViewerPageRouteArgs>();
      return _i3.CustomPage<dynamic>(
          routeData: routeData,
          child:
              _i2.PageViewerPage(itemId: args.itemId, homeData: args.homeData),
          transitionsBuilder: _i5.RouteAnimation.rocketeer,
          durationInMilliseconds: 500,
          reverseDurationInMilliseconds: 500,
          opaque: true,
          barrierDismissible: false);
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig('/#redirect',
            path: '/', redirectTo: 'home', fullMatch: true),
        _i3.RouteConfig(HomePageRoute.name, path: 'home'),
        _i3.RouteConfig(PageViewerPageRoute.name, path: 'page')
      ];
}

/// generated route for [_i1.HomePage]
class HomePageRoute extends _i3.PageRouteInfo<void> {
  const HomePageRoute() : super(name, path: 'home');

  static const String name = 'HomePageRoute';
}

/// generated route for [_i2.PageViewerPage]
class PageViewerPageRoute extends _i3.PageRouteInfo<PageViewerPageRouteArgs> {
  PageViewerPageRoute({required String itemId, required _i6.HomeData homeData})
      : super(name,
            path: 'page',
            args: PageViewerPageRouteArgs(itemId: itemId, homeData: homeData));

  static const String name = 'PageViewerPageRoute';
}

class PageViewerPageRouteArgs {
  const PageViewerPageRouteArgs({required this.itemId, required this.homeData});

  final String itemId;

  final _i6.HomeData homeData;
}
