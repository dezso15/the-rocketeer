import 'package:dio/dio.dart';
import 'package:the_rocketeer/core/interceptors/rocketeer_interceptor.dart';

class _Servers {
  final simpleClub = Dio(
    BaseOptions(
      baseUrl: "https://simpleclub-coding-challenges.web.app/",
      contentType: "application/json",
    ),
  )..interceptors.addAll([RocketeerInterceptor()]);
}

final _Servers servers = _Servers();
