import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:the_rocketeer/core/util/logger.dart';

class RocketeerInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      logger.e('ERROR: $err');
    }
    return super.onError(err, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (kDebugMode) {
      logger.v('REQUEST: ${options.uri} started');
    }
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (kDebugMode) {
      final request = response.requestOptions.uri.toString();
      logger.d("RESPONSE (" + request + "): " + response.data.toString());
    }
    return super.onResponse(response, handler);
  }
}
