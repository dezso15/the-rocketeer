import 'package:flutter/material.dart';

class ErrorSnack extends StatelessWidget {
  final String error;
  const ErrorSnack({required this.error, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      error,
      style: const TextStyle(
        color: Colors.white,
      ),
    );
  }
}
