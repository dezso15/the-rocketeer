import 'dart:math';

import 'package:flutter/material.dart';

enum _AnimationState {
  up,
  down,
}

const max = 200.0;
const min = 0.0;
const reversedInRadian = 250 * (pi / 180);

class LoadingAnimation extends StatefulWidget {
  const LoadingAnimation({Key? key}) : super(key: key);

  @override
  _LoadingAnimationState createState() => _LoadingAnimationState();
}

class _LoadingAnimationState extends State<LoadingAnimation> {
  _AnimationState animationState = _AnimationState.down;

  @override
  Widget build(BuildContext context) {
    late Tween<double> tween;

    switch (animationState) {
      case _AnimationState.up:
        tween = Tween<double>(begin: min, end: max);
        break;
      case _AnimationState.down:
        tween = Tween<double>(begin: max, end: min);
        break;
    }

    return TweenAnimationBuilder<double>(
      tween: tween,
      duration: const Duration(milliseconds: 1200),
      curve: Curves.easeInOut,
      onEnd: () {
        setState(() {
          if (animationState == _AnimationState.up) {
            animationState = _AnimationState.down;
          } else {
            animationState = _AnimationState.up;
          }
        });
      },
      builder: (cont, pos, w) {
        return _AnimationBody(pos: pos, animationState: animationState);
      },
    );
  }
}

class _AnimationBody extends StatelessWidget {
  final double pos;
  final _AnimationState animationState;

  const _AnimationBody({
    required this.pos,
    required this.animationState,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: pos),
            Transform.rotate(
              angle: animationState == _AnimationState.down ? reversedInRadian : 0,
              child: SizedBox(
                width: 100,
                height: 100,
                child: Transform(
                  alignment: Alignment.center,
                  transform: animationState == _AnimationState.down ? Matrix4.rotationY(pi) : Matrix4.rotationY(0),
                  child: Image.asset("assets/images/rocketeer.png"),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: pos),
      ],
    );
  }
}
