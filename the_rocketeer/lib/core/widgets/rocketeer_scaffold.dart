import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';

class RocketeerScaffold extends StatelessWidget {
  final Widget child;
  final bool isScrollable;

  RocketeerScaffold({
    required this.child,
    this.isScrollable = false,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        title: GestureDetector(
          onTap: () {
            AutoRouter.of(context).popUntilRoot();
          },
          child: Image.asset(
            "assets/images/therocketeer_icon.png",
            height: 50,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        height: UIHelper.deviceHeight,
        width: UIHelper.deviceWidth,
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: (isScrollable)
            ? SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    child,
                  ],
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: child),
                ],
              ),
      ),
    );
  }
}
