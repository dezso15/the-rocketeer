import 'package:flutter/material.dart';

enum AnimationState { up, down }

class AnimatedOpenableContainer extends StatefulWidget {
  final double maxHeight;
  final Widget childWidget;
  final GlobalKey<AnimatedOpenableContainerState> key;

  AnimatedOpenableContainer({required this.childWidget, required this.maxHeight, required this.key});

  @override
  AnimatedOpenableContainerState createState() => AnimatedOpenableContainerState();
}

class AnimatedOpenableContainerState extends State<AnimatedOpenableContainer> {
  Tween<double> tween = Tween<double>(begin: 0, end: 0);
  AnimationState animationState = AnimationState.up;

  void changeState() {
    setState(() {
      if (animationState == AnimationState.down) {
        tween = Tween<double>(begin: widget.maxHeight, end: 0);
        animationState = AnimationState.up;
      } else {
        tween = Tween<double>(begin: 0, end: widget.maxHeight);
        animationState = AnimationState.down;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      child: SizedBox(
        height: 0,
        width: double.infinity,
        child: widget.childWidget,
      ),
      tween: tween,
      duration: const Duration(milliseconds: 300),
      builder: (context, double val, widget) {
        return SizedBox(
          height: val,
          child: widget,
          width: double.infinity,
        );
      },
    );
  }
}
