import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

final rocketeerThemeData = ThemeData(
  textTheme: TextTheme(
    headline1: GoogleFonts.workSans(
      fontSize: 30,
      fontWeight: FontWeight.bold,
      letterSpacing: 1.1,
    ),
    bodyText1: GoogleFonts.sourceSerifPro(
      fontSize: 18.0,
      wordSpacing: 4,
      height: 1.55,
    ),
    headline2: GoogleFonts.workSans(
      fontSize: 28,
      color: Colors.black.withOpacity(0.75),
      fontWeight: FontWeight.w600,
    ),
    headline3: GoogleFonts.workSans(
      fontSize: 23,
      color: Colors.black.withOpacity(0.75),
      fontWeight: FontWeight.bold,
    ),
    button: const TextStyle(
      fontSize: 18.0,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
  ),
  cardTheme: CardTheme(
    elevation: 3,
    color: const Color(0xFF26304b),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    ),
  ),
  primaryIconTheme: const IconThemeData(
    color: AppColors.ligthBlue,
    size: 30,
  ),
  backgroundColor: AppColors.ligthBlue,
  iconTheme: const IconThemeData(
    color: Color(0xFF505e85),
    size: 30,
  ),
  appBarTheme: const AppBarTheme(
    color: Color(0xFF333647),
  ),
);
