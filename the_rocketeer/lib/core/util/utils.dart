import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';

class Utils {
  static Future<PaletteGenerator> paletteGeneratorFrom(String? url) async {
    if (url == null) {
      return PaletteGenerator.fromColors([PaletteColor(Colors.white, 1)]);
    }

    PaletteGenerator paletteGenerator = await PaletteGenerator.fromImageProvider(
      Image.network(url).image,
    );
    return paletteGenerator;
  }

  static Color getComplementaryColor(Color color) {
    return Color.fromRGBO(255 - color.red, 255 - color.green, 255 - color.blue, 1);
  }
}
