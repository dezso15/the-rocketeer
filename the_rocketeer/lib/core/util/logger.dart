import 'package:logger/logger.dart';

final logger = Logger(
  printer: PrefixPrinter(
    PrettyPrinter(
      methodCount: 0,
      colors: true,
      printEmojis: true,
    ),
  ),
);
