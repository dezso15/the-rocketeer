import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:the_rocketeer/core/const/theme.dart';
import 'package:the_rocketeer/core/router/router.gr.dart';
import 'package:the_rocketeer/core/util/ui_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: AutoRouterDelegate(
        _appRouter,
        navigatorObservers: () => [AutoRouteObserver()],
      ),
      routeInformationParser: _appRouter.defaultRouteParser(),
      debugShowCheckedModeBanner: false,
      theme: rocketeerThemeData,
      builder: (context, child) {
        UIHelper.deviceHeight = MediaQuery.of(context).size.height;
        UIHelper.deviceWidth = MediaQuery.of(context).size.width;

        return child!;
      },
    );
  }
}
